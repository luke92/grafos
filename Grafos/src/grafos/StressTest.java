package grafos;

public class StressTest
{
	public static void main(String[] args)
	{
		for(int n=400; n<=1000; n+=100)
		{
			Grafo grafo = Generador.aleatorio(n, 0.25);
			
			long repeticiones = 1000;
			long inicio = System.currentTimeMillis();
			
			BFS bfs = new BFS(grafo);
			for(int i=0; i<repeticiones; ++i)
				bfs.esConexo();
			
			long fin = System.currentTimeMillis();
			
			double tiempo = (fin - inicio) / 1000.0 / repeticiones;
			System.out.println("n = " + n + ", Tiempo: " + tiempo + " seg.");
		}
	}
}


















