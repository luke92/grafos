package grafos;

import java.util.Random;

public class Generador
{
	public static Grafo aleatorio(int n, double densidad)
	{
		Grafo ret = new Grafo(n);
		Random random = new Random();
		
		int aristas = (int)(n * (n-1) * densidad / 2);
		for(int k=0; k<aristas; ++k)
		{
			int i = random.nextInt(n); // Entre 0 y n-1
			int j = random.nextInt(n);
			
			if( i != j )
				ret.agregarArista(i,j);
		}
		
		return ret;
	}
}
