package maxClique;

import java.util.HashSet;
import java.util.Set;

import grafos.Grafo;

public class Solver
{
	private Grafo _grafo;
	private Set<Integer> _conjuntoActual;
	private Set<Integer> _cliqueActual;
	
	public Solver(Grafo grafo)
	{
		_grafo = grafo;
	}
	
	public Set<Integer> resolver()
	{
		_conjuntoActual = new HashSet<Integer>();
		_cliqueActual = new HashSet<Integer>();
		
		recursion(0);
		return _cliqueActual;
	}
	
	private void recursion(int inicial)
	{
		// Caso base
		if( inicial == _grafo.tamano())
		{
			if( conjuntoActualEsClique() && _conjuntoActual.size() > _cliqueActual.size() )
				_cliqueActual = clonarConjuntoActual();
			
			return;
		}
		
		// Caso recursivo
		_conjuntoActual.add(inicial);
		recursion(inicial+1);
		
		_conjuntoActual.remove(inicial);
		recursion(inicial+1);
	}
	
	private Set<Integer> clonarConjuntoActual()
	{
		Set<Integer> ret = new HashSet<Integer>();
		for(Integer i: _conjuntoActual)
			ret.add(i);
		
		return ret;
	}

	private boolean conjuntoActualEsClique()
	{
		for(Integer i: _conjuntoActual)
		for(Integer j: _conjuntoActual) if( i != j && !_grafo.existeArista(i, j) )
			return false;
		
		return true;
	}
}















