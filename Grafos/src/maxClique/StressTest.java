package maxClique;

import grafos.Generador;
import grafos.Grafo;

public class StressTest
{
	public static void main(String[] args)
	{
		for(int n=5; n<=50; ++n)
		{
			Grafo grafo = Generador.aleatorio(n, 0.25);
			long inicio = System.currentTimeMillis();
			
			Solver solver = new Solver(grafo);
			solver.resolver();
			
			long fin = System.currentTimeMillis();
			System.out.println("n = " + n + ", Tiempo: " + (fin-inicio)/1000.0);
		}
	}
}
