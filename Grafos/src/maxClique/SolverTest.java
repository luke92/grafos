package maxClique;

import static org.junit.Assert.*;
import grafos.Grafo;
import grafos.Assert;

import org.junit.Test;

public class SolverTest
{
	@Test
	public void casitaTest()
	{
		Grafo casita = new Grafo(5);
		casita.agregarArista(0, 1);
		casita.agregarArista(1, 2);
		casita.agregarArista(2, 3);
		casita.agregarArista(3, 4);
		casita.agregarArista(4, 0);
		casita.agregarArista(1, 3);
		
		int[] clique = { 1, 2, 3 };
		
		Solver solver = new Solver(casita);
		Assert.iguales(clique, solver.resolver());
	}
	
	@Test
	public void completoTest()
	{
		Grafo completo = new Grafo(4);
		completo.agregarArista(0, 1);
		completo.agregarArista(0, 2);
		completo.agregarArista(0, 3);
		completo.agregarArista(1, 2);
		completo.agregarArista(1, 3);
		completo.agregarArista(2, 3);
		
		int[] clique = { 0, 1, 2, 3 };
		
		Solver solver = new Solver(completo);
		Assert.iguales(clique, solver.resolver());
	}
	
	@Test
	public void aisladosTest()
	{
		Grafo completo = new Grafo(4);

		Solver solver = new Solver(completo);
		assertEquals(1, solver.resolver().size());
	}
}













